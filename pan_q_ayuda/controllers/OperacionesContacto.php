<?php
    include("../partials/_header.html");
    
    $posted = false;

    
    
    if (isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["correo"]) && isset($_POST["asunto"]) && isset($_POST['mensaje'])) { 

        
        //CAPTCHA
        $recaptcha = $_POST["g-recaptcha-response"];
     
    	$url = 'https://www.google.com/recaptcha/api/siteverify';
    	$data = array(
    		'secret' => '6Ld3jqgUAAAAAHqlJGfZ1SEKKXe7rPsydiJlrlxU',
    		'response' => $recaptcha
    	);
    	$options = array(
    		'http' => array (
    			'method' => 'POST',
    			'content' => http_build_query($data)
    		)
    	);
    	$context  = stream_context_create($options);
    	$verify = file_get_contents($url, false, $context);
    	$captcha_success = json_decode($verify);
    	
    	if ($captcha_success->success) {

    	    //PARTE DE ENVIAR EL EMAIL
    		$correo=strtolower($_POST['correo']);//mail de cliente
            $nombreCompleto=$_POST['nombre'] . " " . $_POST['apellido'];// nombre del que envia
            $from=$nombreCompleto . " <" . $correo . " >";
            $subject=$_POST["asunto"];
            $message=$_POST['mensaje'];
    
    
            //$correoDestinatario="anayolanda@panqayuda.com.mx";// CORREO PARA PRODUCCION
            $correoDestinatario="a01205935@gmail.com";//POR AHORA PARA HACER PRUEBAS 
    
            require '../libraries/vendor/autoload.php';
    
            # Instantiate the client.
            $mgClient = new \Mailgun\Mailgun('key-ed7fba3df4d87e1ca52524235706da09');
            $domain = "panqayuda.com.mx";
    
            # Make the call to the client.
            $result = $mgClient->sendMessage("$domain",
              array('from'    => $from,
                    'to'      => $correoDestinatario,
                    'subject' => $subject,
                    'text'    => $message));
            
    
             if( $result == true ) {
                $posted = true;
                //echo "Mensaje enviado satisfactoriamente...A Pan Q' Ayuda ";
    
             }else {
                //echo "Error:Message could not be sent...";
                $posted = false;
                
             }
    	} else {

    		//echo "Eres un robot!";
    	}
    	
    	//CAPTCHA


    }else{
        //echo "Faltan datos!!!";
    }
    
    include("../views/contacto.html");

    include("../partials/_footer.html");
?> 